# Studying Semantinc Programming

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

This repository contains the results of my findings studying semantic
programming. It exists primarily as a resource for me, but you are welcome to
do just about anything you want with it. Enjoy.

# License

All the source code produced by me is licensed:

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

Documentation and other code are licensed according to their own licenses.
